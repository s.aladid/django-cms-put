from django.apps import AppConfig


class CmsPutAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cms_put_app'
