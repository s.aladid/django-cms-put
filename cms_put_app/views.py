from django.http import HttpResponseNotAllowed, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

@csrf_exempt
@require_http_methods(["GET", "PUT"])
def content_view(request, resource=None):
    if request.method == 'GET':
        if resource is None:
            return HttpResponse("Welcome to the CMS!", status=200)
        else:
            return get_content(request, resource)
    elif request.method == 'PUT':
        return put_content(request, resource)
    else:
        return HttpResponseNotAllowed(['GET', 'PUT'])

def get_content(request, resource):
    contents = request.session.get('contents', {})
    content = contents.get(resource)
    if content:
        page = PAGE.format(content=content)
        return HttpResponse(page, status=200)
    else:
        return HttpResponseNotFound(PAGE_NOT_FOUND.format(resource=resource))

def put_content(request, resource):
    if request.body:
        contents = request.session.get('contents', {})
        contents[resource] = request.body.decode('utf-8')
        request.session['contents'] = contents
        return HttpResponse(PAGE.format(content=request.body.decode('utf-8')), status=200)
    else:
        return HttpResponse("No content provided", status=400)